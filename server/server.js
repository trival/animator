var animator;

animator = {};

animator.functions = {};

animator.server = {};

animator.server.routes = {};

animator.server.config = {
  CLIENT_DIR: __dirname + '/../public',
  SERVER_PORT: 7070
};

animator.server.libs = {
  express: require('express'),
  bacon: require('baconjs')
};

animator.server.storage = {
  users: [],
  animations: []
};

animator.server.routes = {
  getAnimation: function(req, res) {
    var animation;
    animation = animator.server.storage.animations[req.params.id];
    if (animation) {
      return res.json({
        result: animation
      });
    } else {
      return res.json({
        error: "animation not found"
      });
    }
  },
  saveAnimation: function(req, res) {
    var animation;
    animation = JSON.parse(req.body);
    if (animation) {
      animator.server.storage.animations[animation.id] = animation;
      return res.json({
        result: true
      });
    } else {
      return res.json({
        error: "no valid animation transmitted"
      });
    }
  }
};

animator.server.routes.init = function() {
  var app, routes, _ref;
  _ref = animator.server, app = _ref.app, routes = _ref.routes;
  app.get('/get-animation', routes.getAnimation);
  return app.post('/post-animation', routes.saveAnimation);
};

animator.server.app = (function() {
  var CLIENT_DIR, app, express;
  express = animator.server.libs.express;
  CLIENT_DIR = animator.server.config.CLIENT_DIR;
  app = express();
  app.configure(function() {
    app.use(express.bodyParser());
    app.use(app.router);
    app.use(express["static"](CLIENT_DIR));
    return app.use(express.errorHandler({
      dumpExceptions: true,
      showStack: true
    }));
  });
  return app;
})();

animator.server.start = function() {
  var app, config, routes, _ref;
  _ref = animator.server, config = _ref.config, app = _ref.app, routes = _ref.routes;
  routes.init();
  app.listen(config.SERVER_PORT);
  return console.log("server started on port " + config.SERVER_PORT);
};

animator.server.start();
