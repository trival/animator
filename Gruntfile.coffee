module.exports = (grunt) ->

  # load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach grunt.loadNpmTasks

  #concatenated Modules
  modulesClient = grunt.file.readJSON 'modules_client.json'
  modulesServer = grunt.file.readJSON 'modules_server.json'

  grunt.initConfig

    buildTempDir: "build/tmp/"
    buildDistDir: "build/dist/"
    bowerLibs: "bower_modules/"
    modules: "<%= buildTempDir %>src/"

    watch:
      options:
        livereload: true

      coffee:
        files: ['src/**/*.coffee', 'src/**/*.js']
        tasks: ['build', 'distJs']

      less:
        files: ['src/style/{,*/}*.less']
        tasks: ['less', 'concat:css' ]

      html:
        files: ['public/**/*.html']
        tasks: []


    clean:
      dist: [
        'public/script/**/*'
        'public/style/*'
        'public/font/'
        'server/server.js'
        '<%= buildTempDir %>'
        '<%= buildDistDir %>**/*'
      ]


    less:
      css:
        src: 'src/style/main.less'
        dest: '<%= buildDistDir %>style/main.css'


    copy:
      js:
        expand: true
        cwd: 'src/'
        src: '**/*.js'
        filter: 'isFile'
        dest: '<%= buildTempDir %>src/'

      dist:
        files:
          'public/script/app.js': '<%= buildDistDir %>app.js'
          'public/script/libs.js': '<%= buildDistDir %>libs.js'
          'server/server.js': '<%= buildDistDir %>server.js'

      build: # Here should be the compression step
        files:
          '<%= buildDistDir %>app.js': '<%= buildDistDir %>app.build.js'
          '<%= buildDistDir %>server.js': '<%= buildDistDir %>server.build.js'

      assetFonts:
        expand: true
        cwd: 'bower_modules/font-awesome/font/'
        src: '*'
        flatten: true
        filter: 'isFile'
        dest: 'public/font/'


    concat:
      css:
        src: [
          '<%= buildDistDir %>style/*'
        ]
        dest: 'public/style/style.css'

      js:
        files:
          '<%= buildDistDir %>app.build.js': modulesClient
          '<%= buildDistDir %>server.build.js': modulesServer

      libs:
        src: [
          '<%= bowerLibs %>jquery/dist/jquery.min.js'
          '<%= bowerLibs %>lodash/dist/lodash.min.js'
          '<%= bowerLibs %>bacon/dist/Bacon.min.js'
          '<%= bowerLibs %>react/react.min.js'
        ]
        dest: '<%= buildDistDir %>libs.js'


    coffee:
      app:
        options:
          bare: true
        files: [
          expand: true
          src: 'src/**/*.coffee'
          dest: '<%= buildTempDir %>'
          ext: '.js'
        ]


    nodemon:
      server:
        script: 'server/server.js'
        options:
          watchedExtensions: ['js']
          watchedFolders: ['server']
          debug: true
          delayTime: 1


    concurrent:
      target:
        tasks: ['nodemon', 'watch']
        options:
          logConcurrentOutput: true


    mochaTest:
      node:
        src: ['test/nodejs/**/*.coffee', 'test/shared/**/*.coffee']
        options:
          reporter: 'spec'
          ui: 'tdd'
          timeout: 5000
          require: 'test/nodejs-setup.js'


    karma:
      unit:
        configFile: 'karma.conf.coffee'


  # build/dist tasks
  grunt.registerTask 'build', ['coffee', 'copy:js', 'concat:js']
  grunt.registerTask 'distResources', ['less', 'copy:assetFonts', 'concat:css', 'concat:libs']
  grunt.registerTask 'distJs', ['copy:build', 'copy:dist']
  grunt.registerTask 'distDev', ['clean', 'build', 'distResources', 'distJs']

  # test tasks
  grunt.registerTask 'test', ['mochaTest', 'karma']

  # automated dev tasks
  grunt.registerTask 'default', ['distDev', 'concurrent']
