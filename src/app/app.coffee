CELL_COUNT = 20
CELL_WIDTH = 20
frames = []
screen = document.getElementById("screen")
screen.width = screen.height = CELL_COUNT * CELL_WIDTH
ctx = screen.getContext("2d")
x0 = screen.offsetLeft
y0 = screen.offsetTop

negate = (v) -> not v

id = (v) -> v


screenIndex = ([x, y]) ->
  y * CELL_COUNT + x


screenPos = (index) ->
  x = index % CELL_COUNT
  y = Math.floor index / CELL_COUNT
  [x, y]


cellId = (x, y) ->
  "px" + x + "_" + y


toCellPos = (e) ->
  x = e.clientX - x0 + document.body.scrollLeft
  y = e.clientY - y0 + document.body.scrollTop
  x = Math.floor(x / CELL_WIDTH)
  y = Math.floor(y / CELL_WIDTH)
  [x, y]


isEqualPos = ([x1, y1], [x2, y2]) ->
  x1 is x2 and y1 is y2


changeFrameIndex = (prev, {length, offset}) ->
  prev = length - 1 if prev >= length
  prev = length if prev <= 0
  (prev + offset) % length


makeNextIndexObj = (offset) -> (length) ->
  length: length
  offset: offset


updateFramesLength = (old, next) ->
  Math.max 1, old + next


copyFrame = (oldFrame) ->
  newFrame = createFrame()
  for val, i in oldFrame
    newFrame[i] = val
  newFrame


createFrame = ->
  new Uint8Array(CELL_COUNT * CELL_COUNT)


# frame callbacks
intoFrames = (frame, index) ->
  frames.splice index, 0, frame
  return


deleteFrame = (index) ->
  frames.splice index, 1
  return


# callbacks
drawScreen = (frame) ->
  for cellOn, i in frame
    pos = screenPos i
    drawCell pos, cellOn
  return


updateCell = (pos, frame) ->
  i = screenIndex pos
  cellOn = frame[i] = not frame[i]
  drawCell pos, cellOn
  return


drawCell = ([x, y], cellOn) ->
  x *= CELL_WIDTH
  y *= CELL_WIDTH
  if cellOn
    ctx.fillStyle = "#000"
  else
    ctx.fillStyle = "#fff"
  ctx.fillRect x, y, CELL_WIDTH, CELL_WIDTH
  return


# init frames
frames.push createFrame()


# Event Streams
mouseDown = $ screen
            .asEventStream "mousedown"
            .doAction ".preventDefault"

mouseUp = $ document
          .asEventStream "mouseup"
          .doAction ".preventDefault"

mouseMoves = $ screen
             .asEventStream "mousemove"
             .map toCellPos
             .skipDuplicates isEqualPos

clicks = mouseDown.map toCellPos

cellTouches = mouseDown.flatMap ->
                mouseMoves.takeUntil mouseUp
              .merge clicks

newStream = $ "#new-btn"
            .asEventStream "click"

copyStream = $ "#copy-btn"
             .asEventStream "click"

deleteStream = $ "#del-btn"
               .asEventStream "click"

nextIndex = $ "#next-btn"
            .asEventStream "click"

prevIndex = $ "#prev-btn"
            .asEventStream "click"

play = $ "#play-btn"
       .asEventStream "click"
       .scan false, negate
       .changes()

stop = play.filter negate

animation = play.filter id
            .flatMap ->
              Bacon.interval 200, 1
              .takeUntil stop

framesLengthBus = new Bacon.Bus()

framesLength = framesLengthBus.toProperty frames.length

currentIndex = nextIndex.merge animation
               .map framesLength
               .map makeNextIndexObj 1
               .merge (prevIndex.map framesLength
                                .map makeNextIndexObj -1)
               .merge framesLengthBus.map makeNextIndexObj 0
               .scan 0, changeFrameIndex

frame = currentIndex.map (i) -> frames[i]

changes = frame.sampledBy cellTouches,
          (frame, pos) -> frame: frame, pos: pos


# callback registration
frame.onValue drawScreen

changes.onValue (o) ->
  updateCell o.pos, o.frame
  return

newStream.map currentIndex
         .onValue (i) ->
           intoFrames createFrame(), i
           framesLengthBus.push frames.length
           return

copyStream.map currentIndex
          .onValue (i) ->
            copy = copyFrame frames[i]
            intoFrames copy, i
            framesLengthBus.push frames.length
            return

deleteStream.map currentIndex
            .onValue (i) ->
              if frames.length > 1
                deleteFrame i
                framesLengthBus.push frames.length
              return

currentIndex.onValue (v) ->
  console.log "currentIndex", v
  return

framesLength.onValue (v) ->
  console.log "framesLength", v
  return

animator.start = id
