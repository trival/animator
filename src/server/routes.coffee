animator.server.routes =

  getAnimation: (req, res) ->
    animation = animator.server.storage.animations[req.params.id]
    if animation
      res.json result: animation
    else
      res.json error: "animation not found"


  saveAnimation: (req, res) ->
    animation = JSON.parse req.body
    if animation
      animator.server.storage.animations[animation.id] = animation
      res.json result: true
    else
      res.json error: "no valid animation transmitted"



animator.server.routes.init = ->
  {app, routes} = animator.server

  app.get '/get-animation', routes.getAnimation
  app.post '/post-animation', routes.saveAnimation
