animator.server.app = do ->
  {express} = animator.server.libs
  {CLIENT_DIR} = animator.server.config

  app = express()

  app.configure ->
    app.use express.bodyParser()
    app.use app.router
    app.use express.static CLIENT_DIR
    app.use express.errorHandler dumpExceptions: true, showStack: true

  app


animator.server.start = ->
  {config, app, routes} = animator.server
  
  routes.init()
  app.listen config.SERVER_PORT
  console.log "server started on port " + config.SERVER_PORT

